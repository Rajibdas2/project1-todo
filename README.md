# SPA with Vue Cli and Laravel and JWT Token Based Application
## Requirements

- Laravel 9
- nodejs 12.14
- PHP >= 8.0
- MySQL >= 5.7
- OpenSSL PHP Extension
- PDO PHP Extension
- Mbstring PHP Extension

## Installation
Open Terminal and Clone the repository
```
https://gitlab.com/Rajibdas2/project1-todo.git
```
### Backend Configuration
```
$ cd backend
$ composer install
$ cp .env.example .env
```
Set value on the following environment variable
- APP_KEY
- DB_DATABASE
- DB_USERNAME
- DB_PASSWORD


-JWT_SECRET=api_token

-AUTH_COOKIE_NAME=csrf

-JWT_TTL=86400
```



$ php artisan migrate --seed
$ php artisan serve
```
Now your API host is running on http://localhost:8000/

### Frontend Configuration
```
$ cd ..
$ cd frontend
$ npm install
$ npm run serve
```
Now your frontend is running on http://localhost:8080/

## Default Credentials
- Email' => 'test_user1@gmail.com',
- Password : 12345678
- Email' => 'test_user2@gmail.com',
- Password : 12345678

