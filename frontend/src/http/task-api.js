import api from "@/http/api";

const resource = "/todos"
export const allTodos = () => api.get(resource)
export const createTodo = (todo) => api.post(resource, todo)

export const updatetodo = (id, todo) => api.put(`${resource}/{id}`, todo)

export const removeTodo = (id) => api.delete(`${resource}/{id}`)

export const completeTodo = (id, todo) => api.put(`${resource}/{id}/complete`, todo)


