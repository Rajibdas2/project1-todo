<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTodoRequest;
use App\Http\Resources\TodoResource;
use App\Models\Todo;
use Illuminate\Http\Request;

class TodosController extends Controller
{
    public function __construct()
    {
        $this->middleware('jwt.verify');
    }

    public function index()
    {
        $todos = Todo::where('user_id', auth()->id())->orderBy('created_at','desc')->get();
        return $todos;
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $data = $request->only(['name']);
        $data['user_id'] = auth()->id();
        Todo::create($data);
        return response()->json(['success' => true, 'message' => 'Todo Successfully created.']);

    }

    /**
     * Display the specified resource.
     */
    public function show(Todo $todo)
    {
        $todo = Todo::where('id', $todo)->where('user_id', auth()->id())->first();
        if($todo)
            return $todo;
        return response()->json(['success' => false, 'message' => 'Todo is not found.'], 412);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(StoreTodoRequest $request, Todo $todo)
    {
        $todo->update($request->all());
        return new TodoResource($todo);
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Todo $todo){
        $todo->delete();
        return response()->json(['success' => true, 'message' => 'Todo Successfully deleted.']);

    }

    public function sendResponse($data, $message, $status = 200){
        $response = [
            'data' => $data,
            'message' => $message
        ];
        return response()->json($response, $status);
    }
}
