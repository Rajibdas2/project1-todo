<?php

    use App\Http\Controllers\AuthController;
    use Illuminate\Support\Facades\Route;


//API route for register and login
    Route::post('/register', [AuthController::class, 'register']);
    Route::post('/login', [AuthController::class, 'login']);

//Protecting Routes
    Route::group(['middleware' => ['jwt.verify']], function() {
        Route::post('logout', [AuthController::class, 'logout']);
        Route::get('get_user', [AuthController::class, 'get_user']);
        Route::apiResource('todos',\App\Http\Controllers\Api\TodosController::class);

    });

